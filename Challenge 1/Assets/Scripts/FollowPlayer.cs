﻿using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    private readonly Vector3 _offset = new Vector3(30, 0, 10);
    
    public GameObject plane;

    public void Update()
    {
        transform.position = plane.transform.position + _offset;
    }
}
