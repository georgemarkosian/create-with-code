﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 15;
    public float rotationSpeed = 60;
    public float verticalInput;

    private void FixedUpdate()
    {
        verticalInput = Input.GetAxis("Vertical");

        transform.Translate(Vector3.forward * Time.deltaTime * speed);
        transform.Rotate(Vector3.left * Time.deltaTime * rotationSpeed * verticalInput);
    }
}
