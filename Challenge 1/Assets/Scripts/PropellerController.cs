﻿using UnityEngine;

public class PropellerController : MonoBehaviour
{
    private const float ROTATE_SPEED = 20;
    
    private void Update()
    {
        transform.Rotate(0, 0, ROTATE_SPEED, Space.Self);
    }
}
