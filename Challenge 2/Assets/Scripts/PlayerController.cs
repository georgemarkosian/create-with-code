﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject dogPrefab;
    public float dogSpawnInterval = 1;

    private float lastDogSpawnTime;
    
    private void Update()
    {
        if (!Input.GetKeyDown(KeyCode.Space))
            return;

        if (lastDogSpawnTime > 0 && Time.time - lastDogSpawnTime < dogSpawnInterval)
        {
            Debug.Log("Dogs spawning thing is on timeout.");
            return;
        }
        
        lastDogSpawnTime = Time.time;
        Instantiate(dogPrefab, transform.position, dogPrefab.transform.rotation);
    }
}
