﻿using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject[] ballPrefabs;

    private float spawnLimitXLeft = -22;
    private float spawnLimitXRight = 7;
    private float spawnPosY = 30;

    private float startDelay = 1.0f;
    private float spawnIntervalMin = 3.0f;
    private float spawnIntervalMax = 5.0f;

    private void Start()
    {
        Invoke(nameof(SpawnRandomBall), startDelay);
    }

    private void SpawnRandomBall ()
    {
        var ballPrefabIndex = Random.Range(0, ballPrefabs.Length);
        var spawnPos = new Vector3(Random.Range(spawnLimitXLeft, spawnLimitXRight), spawnPosY, 0);
        var ballPrefab = ballPrefabs[ballPrefabIndex];
        Instantiate(ballPrefab, spawnPos, ballPrefab.transform.rotation);

        var interval = Random.Range(spawnIntervalMin, spawnIntervalMax);
        Invoke(nameof(SpawnRandomBall), interval);
    }
}
