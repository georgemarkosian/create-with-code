﻿using UnityEngine;

public class MoveLeft : MonoBehaviour
{
    private const float BOUND_LEFT = -10f;
    
    public float speed;
    
    private PlayerController _playerControllerScript;

    private void Start()
    {
        _playerControllerScript = GameObject.Find("Player").GetComponent<PlayerController>();
    }

    private void Update()
    {
        if (!_playerControllerScript.IsGameOver)
            transform.Translate(Vector3.left * speed * Time.deltaTime, Space.World);

        if (transform.position.x < BOUND_LEFT && !gameObject.CompareTag("Background"))
            Destroy(gameObject);
    }
}
