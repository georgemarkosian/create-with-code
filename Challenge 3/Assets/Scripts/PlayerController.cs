﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float floatForce;
    public float bounceForce;
    public float gravityModifier = 1.5f;
    
    public ParticleSystem explosionParticle;
    public ParticleSystem fireworksParticle;

    public AudioClip moneySound;
    public AudioClip explodeSound;
    public AudioClip bounceSound;

    private bool _isGameOver;

    private Rigidbody _playerRigidBody;
    private AudioSource _playerAudio;

    public bool IsGameOver => _isGameOver;
    
    /*
     * Unity events.
     */
    
    private void Start()
    {
        Physics.gravity *= gravityModifier;
        
        _playerAudio = GetComponent<AudioSource>();
        _playerRigidBody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        TryFlip();
        CheckPosition();
    }

    private void OnCollisionEnter(Collision other)
    {
        if (_isGameOver)
            return;
        
        if (other.gameObject.CompareTag("Bomb"))
        {
            _isGameOver = true;
            _playerAudio.PlayOneShot(explodeSound, 1.0f);
            
            explosionParticle.Play();
            
            Destroy(other.gameObject);
            
            Debug.Log("Game Over!");
        } 
        else if (other.gameObject.CompareTag("Money"))
        {
            fireworksParticle.Play();
            _playerAudio.PlayOneShot(moneySound, 1.0f);
            Destroy(other.gameObject);
        }
        else if (other.gameObject.CompareTag("Ground"))
        {
            _playerAudio.PlayOneShot(bounceSound, 1.0f);
            _playerRigidBody.AddForce(Vector3.up * bounceForce);
        }
    }
    
    /*
     * Helpers.
     */

    private void TryFlip()
    {
        if (_isGameOver)
            return;
        
        if (!Input.GetKey(KeyCode.Space))
            return;
        
        if (gameObject.transform.position.y < 16.4f)
            _playerRigidBody.AddForce(Vector3.up * floatForce);
    }

    private void CheckPosition()
    {
        if (_isGameOver)
            return;

        if (gameObject.transform.position.y >= 16.4f)
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, 16.4f, gameObject.transform.position.z);
    }
}
