﻿using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject[] objectPrefabs;
    
    private float _spawnDelay = 2f;
    private float _spawnInterval = 1.5f;

    private PlayerController _playerControllerScript;

    private void Start()
    {
        _playerControllerScript = GameObject.Find("Player").GetComponent<PlayerController>();
        
        InvokeRepeating(nameof(SpawnObjects), _spawnDelay, _spawnInterval);
    }

    private void SpawnObjects ()
    {
        if (_playerControllerScript.IsGameOver)
            return;
        
        var spawnLocation = new Vector3(39, Random.Range(5, 15), 0);
        var index = Random.Range(0, objectPrefabs.Length);
        Instantiate(objectPrefabs[index], spawnLocation, objectPrefabs[index].transform.rotation);
    }
}
