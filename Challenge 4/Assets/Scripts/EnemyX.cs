﻿using UnityEngine;

public class EnemyX : MonoBehaviour
{
    private float _speed;
    
    private Rigidbody _enemyRigidBody;
    private GameObject _playerGoal;

    /*
     * Unity Events.
     */
    
    private void Start()
    {
        _enemyRigidBody = GetComponent<Rigidbody>();
        _playerGoal = GameObject.Find("Player Goal");
    }

    private void Update()
    {
        var lookDirection = (_playerGoal.transform.position - transform.position).normalized;
        _enemyRigidBody.AddForce(lookDirection * _speed * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.name == "Enemy Goal" || other.gameObject.name == "Player Goal")
            Destroy(gameObject);
    }
    
    /*
     * Public.
     */

    public void SetSpeed(float speed)
    {
        _speed = speed;
    }
}
