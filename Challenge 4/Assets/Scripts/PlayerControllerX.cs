﻿using System.Collections;
using UnityEngine;

public class PlayerControllerX : MonoBehaviour
{
    public GameObject powerUpIndicator;
    public ParticleSystem dashParticle;
    
    public float speed = 500;
    public float strengthNormal = 10;
    public float strengthPowerUp = 25;
    public float dashPower;

    public bool isPowerUpActive;
    public int powerUpDuration = 5;
    
    private Rigidbody _playerRigidBody;
    private GameObject _focalPoint;

    private void Start()
    {
        _playerRigidBody = GetComponent<Rigidbody>();
        _focalPoint = GameObject.Find("FocalPoint");
    }

    private void Update()
    {
        CheckDash();
        UpdateMovement();
        UpdatePowerUpPosition();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("PowerUp"))
            return;
        ActivatePowerUp();
        Destroy(other.gameObject);
    }
    
    private void OnCollisionEnter(Collision other)
    {
        if (!other.gameObject.CompareTag("Enemy")) 
            return;
        var enemyRigidBody = other.gameObject.GetComponent<Rigidbody>();
        var awayFromPlayer = other.gameObject.transform.position - transform.position;
        var strength = isPowerUpActive ? strengthPowerUp : strengthNormal;
        enemyRigidBody.AddForce(awayFromPlayer * strength, ForceMode.Impulse);
    }
    
    /*
     * Movement.
     */

    private void UpdateMovement()
    {
        var verticalInput = Input.GetAxis("Vertical");
        _playerRigidBody.AddForce(_focalPoint.transform.forward * verticalInput * speed * Time.deltaTime);
    }

    /*
     * Dash.
     */

    private void CheckDash()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            _playerRigidBody.AddForce(_focalPoint.transform.forward * dashPower);
            dashParticle.Play();
        }
    }

    /*
     * Power Up.
     */

    private void ActivatePowerUp()
    {
        isPowerUpActive = true;
        powerUpIndicator.SetActive(true);
        StartCoroutine(PowerUpCooldown());
    }

    private IEnumerator PowerUpCooldown()
    {
        yield return new WaitForSeconds(powerUpDuration);
        isPowerUpActive = false;
        powerUpIndicator.SetActive(false);
    }

    private void UpdatePowerUpPosition()
    {
        if (isPowerUpActive)
            powerUpIndicator.transform.position = transform.position + new Vector3(0, -0.6f, 0);
    }
}
