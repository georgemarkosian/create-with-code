﻿using UnityEngine;

public class RotateCameraX : MonoBehaviour
{
    public GameObject player;
    public float speed = 200;

    private void Update()
    {
        var horizontalInput = Input.GetAxis("Horizontal");
        transform.Rotate(Vector3.up, horizontalInput * speed * Time.deltaTime);
        transform.position = player.transform.position;
    }
}
