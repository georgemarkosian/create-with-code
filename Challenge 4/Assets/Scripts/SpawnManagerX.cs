﻿using UnityEngine;

public class SpawnManagerX : MonoBehaviour
{
    public GameObject enemyPrefab;
    public GameObject powerUpPrefab;

    private float spawnRangeX = 10;
    private float spawnZMin = 15; // set min spawn Z
    private float spawnZMax = 25; // set max spawn Z

    public int enemyCount;
    public int waveCount = 1;

    public GameObject player; 

    private void Update()
    {
        enemyCount = GameObject.FindGameObjectsWithTag("Enemy").Length;
        if (enemyCount == 0)
        {
            ResetPlayerPosition();
            SpawnPowerUp();
            SpawnEnemyWave(waveCount++);
        }
    }

    private Vector3 GenerateSpawnPosition()
    {
        var xPos = Random.Range(-spawnRangeX, spawnRangeX);
        var zPos = Random.Range(spawnZMin, spawnZMax);
        return new Vector3(xPos, 0, zPos);
    }
    
    private void SpawnEnemyWave(int enemiesToSpawn)
    {
        for (var i = 0; i < enemiesToSpawn; i++)
        {
            var enemy = Instantiate(enemyPrefab, GenerateSpawnPosition(), enemyPrefab.transform.rotation);
            enemy.GetComponent<EnemyX>().SetSpeed(150 + 100 * enemiesToSpawn);
        }
    }

    private void SpawnPowerUp()
    {
        var powerUpSpawnOffset = new Vector3(0, 0, -15);
        if (GameObject.FindGameObjectsWithTag("PowerUp").Length == 0)
            Instantiate(powerUpPrefab, GenerateSpawnPosition() + powerUpSpawnOffset, powerUpPrefab.transform.rotation);
    }
    
    private void ResetPlayerPosition()
    {
        var playerRigidBody = player.GetComponent<Rigidbody>();
        playerRigidBody.velocity = Vector3.zero;
        playerRigidBody.angularVelocity = Vector3.zero;
        
        player.transform.position = new Vector3(0, 1, -7);
    }
}
