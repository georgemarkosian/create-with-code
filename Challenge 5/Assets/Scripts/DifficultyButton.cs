﻿using UnityEngine;
using UnityEngine.UI;

public class DifficultyButton : MonoBehaviour
{
    public int difficulty;

    private GameManagerX _gameManager;
    private Button _button;

    private void Start()
    {
        _gameManager = GameObject.Find("Game Manager").GetComponent<GameManagerX>();
        
        _button = GetComponent<Button>();
        _button.onClick.AddListener(SetDifficulty);
    }

    private void SetDifficulty()
    {
        _gameManager.StartGame(difficulty);
    }
}
