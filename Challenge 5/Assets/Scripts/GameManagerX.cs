﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameManagerX : MonoBehaviour
{
    private const float SPAWN_RATE_DEFAULT = 1.5f;
    private const float TIME_DEFAULT_SECONDS = 10;
    
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI timeText;
    public TextMeshProUGUI gameOverText;
    
    public GameObject titleScreen;
    public Button restartButton; 

    public List<GameObject> targetPrefabs;

    private float _spawnRate = 1.5f;
    private float _timeLeft;
    private int _score;
    
    public bool isGameActive;

    private float spaceBetweenSquares = 2.5f; 
    private float minValueX = -3.75f; //  x value of the center of the left-most square
    private float minValueY = -3.75f; //  y value of the center of the bottom-most square
    
    /*
     * Public.
     */
    
    public void StartGame(float difficulty)
    {
        _spawnRate = SPAWN_RATE_DEFAULT / difficulty;
        _timeLeft = TIME_DEFAULT_SECONDS;
        _score = 0;
        
        isGameActive = true;
        
        titleScreen.SetActive(false);

        UpdateTimer();
        UpdateScore(0);
        
        StartCoroutine(SpawnTarget());
        StartCoroutine(Countdown());
    }
    
    public void UpdateScore(int scoreToAdd)
    {
        if (!isGameActive)
            return;
        _score = Math.Max(_score + scoreToAdd, 0);
        scoreText.text = $"Score: {_score}";
    }
    
    public void GameOver()
    {
        if (!isGameActive)
            return;
        isGameActive = false;
        gameOverText.gameObject.SetActive(true);
        restartButton.gameObject.SetActive(true);
    }
    
    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    /*
     * Spawning.
     */
    
    private IEnumerator SpawnTarget()
    {
        while (isGameActive)
        {
            yield return new WaitForSeconds(_spawnRate);
            var index = Random.Range(0, targetPrefabs.Count);
            var prefab = targetPrefabs[index];
            if (isGameActive)
                Instantiate(prefab, RandomSpawnPosition(), prefab.transform.rotation);
        }
    }
    
    /*
     * Timer.
     */

    private IEnumerator Countdown()
    {
        while (isGameActive)
        {
            yield return new WaitForSeconds(1);
            
            _timeLeft = Math.Max(_timeLeft - 1, 0);
            UpdateTimer();
            
            if (_timeLeft <= 0)
                GameOver();
        }
    }

    private void UpdateTimer()
    {
        timeText.text = $"Time: {Math.Round(_timeLeft)}";
    }
    
    /*
     * Helpers.
     */

    private Vector3 RandomSpawnPosition()
    {
        var spawnPosX = minValueX + (RandomSquareIndex() * spaceBetweenSquares);
        var spawnPosY = minValueY + (RandomSquareIndex() * spaceBetweenSquares);
        var spawnPosition = new Vector3(spawnPosX, spawnPosY, 0);
        return spawnPosition;
    }

    private int RandomSquareIndex()
    {
        return Random.Range(0, 4);
    }
}
