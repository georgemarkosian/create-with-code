﻿using System.Collections;
using UnityEngine;

public class TargetX : MonoBehaviour
{
    private Rigidbody rb;
    private GameManagerX gameManagerX;
    public int pointValue;
    public GameObject explosionFx;

    public float timeOnScreen = 1.0f;

    private float minValueX = -3.75f; // the x value of the center of the left-most square
    private float minValueY = -3.75f; // the y value of the center of the bottom-most square
    private float spaceBetweenSquares = 2.5f; // the distance between the centers of squares on the game board
    
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        gameManagerX = GameObject.Find("Game Manager").GetComponent<GameManagerX>();

        transform.position = RandomSpawnPosition(); 
        StartCoroutine(RemoveObjectRoutine()); // begin timer before target leaves screen

    }

    /*
     * Event Handlers.
     */
    
    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);

        if (other.gameObject.CompareTag("Sensor") && !gameObject.CompareTag("Bad"))
            gameManagerX.GameOver();
    }
    
    private void OnMouseDown()
    {
        if (!gameManagerX.isGameActive)
            return;
        
        gameManagerX.UpdateScore(pointValue);
        Destroy(gameObject);
        Explode();
    }
    
    /*
     * Helpers.
     */

    private Vector3 RandomSpawnPosition()
    {
        var spawnPosX = minValueX + (RandomSquareIndex() * spaceBetweenSquares);
        var spawnPosY = minValueY + (RandomSquareIndex() * spaceBetweenSquares);
        var spawnPosition = new Vector3(spawnPosX, spawnPosY, 0);
        return spawnPosition;
    }

    private int RandomSquareIndex ()
    {
        return Random.Range(0, 4);
    }

    private void Explode ()
    {
        Instantiate(explosionFx, transform.position, explosionFx.transform.rotation);
    }

    // After a delay, Moves the object behind background so it collides with the Sensor object
    private IEnumerator RemoveObjectRoutine()
    {
        yield return new WaitForSeconds(timeOnScreen);
        if (gameManagerX.isGameActive)
            transform.Translate(Vector3.forward * 5, Space.World);
    }
}
