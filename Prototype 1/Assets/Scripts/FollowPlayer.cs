﻿using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    [SerializeField] private GameObject vehicle;
    [SerializeField] private Vector3 _offset = new Vector3(0, 5, -7);
    
    private void LateUpdate()
    {
        transform.position = vehicle.transform.position + _offset;
    }
}
