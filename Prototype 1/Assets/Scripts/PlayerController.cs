﻿using System;
using TMPro;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private const float TURN_SPEED = 50f;
    
    [SerializeField] private TextMeshProUGUI _speedText;
    [SerializeField] private TextMeshProUGUI _rpmText;
    
    [SerializeField] private float _horsePower;
    [SerializeField] private GameObject _centerOfMass;
    
    private Rigidbody _rigidBody;

    private void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();
        _rigidBody.centerOfMass = _centerOfMass.transform.position;
    }
    
    private void FixedUpdate()
    {
        var horizontalInput = Input.GetAxis("Horizontal");
        var verticalInput = Input.GetAxis("Vertical");
        
        _rigidBody.AddRelativeForce(Vector3.forward * _horsePower * verticalInput);
        transform.Rotate(Vector3.up * Time.deltaTime * TURN_SPEED * horizontalInput);

        var speed = Math.Round(_rigidBody.velocity.magnitude * 2.237);
        var rpm = Mathf.Round((float)(speed % 30 * 40));
        
        _rpmText.text = $"RPM: {rpm}";
        _speedText.SetText($"Speed: {speed}");
    }
}
