﻿using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public float spawnRangeX = 14;
    public float spawnPosZ = 20;

    public float spawnStartDelay = 2;
    public float spawnInterval = 1.5f;
    
    public GameObject[] animalsPrefabs;

    private void Start()
    {
        InvokeRepeating(nameof(SpawnRandomAnimal), spawnStartDelay, spawnInterval);
    }
    
    private void SpawnRandomAnimal()
    {
        var animalIndex = Random.Range(0, animalsPrefabs.Length);
        var animalPrefab = animalsPrefabs[animalIndex];
        var spawnPosition = new Vector3(Random.Range(-spawnRangeX, spawnRangeX), 0, spawnPosZ);
        Instantiate(animalPrefab, spawnPosition, animalPrefab.transform.rotation);
    }
}
