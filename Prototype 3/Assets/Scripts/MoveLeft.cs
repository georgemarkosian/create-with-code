﻿using UnityEngine;

public class MoveLeft : MonoBehaviour
{
    private float speed = 15;
    private float leftBound = -10;
    
    private PlayerController playerControllerScript;

    private void Start()
    {
        playerControllerScript = GameObject.Find("Player").GetComponent<PlayerController>();
    }
    
    private void Update()
    {
        if (!playerControllerScript.isGameOver)
            transform.Translate(Vector3.left * Time.deltaTime * speed);
        
        if (transform.position.x < leftBound && (gameObject.CompareTag("Obstacle") || gameObject.CompareTag("Loot")))
            Destroy(gameObject);
    }
}
