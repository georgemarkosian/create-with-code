﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private const float STRAFE_SPEED = 10f;
    private const float Z_RANGE = 3f;
    private const float X_MIN = 2.2f;
    private const float X_MAX = 7.0f;
    
    public ParticleSystem explosionParticle;
    public ParticleSystem dirtParticle;
    
    public AudioClip jumpSound;
    public AudioClip crashSound;
    public AudioClip lootSound;

    public Text countText;
    
    public float jumpForce = 10f;
    public float gravityModifier = 1f;

    public bool isOnGround = true;
    public bool isGameOver;

    private Rigidbody playerRigidBody;
    private Animator playerAnimation;
    private AudioSource playerAudio;

    private int lootCount;

    /// <summary>
    /// Unity Events.
    /// </summary>
    
    private void Start()
    {
        playerRigidBody = GetComponent<Rigidbody>();
        playerAnimation = GetComponent<Animator>();
        playerAudio = GetComponent<AudioSource>();
        
        Physics.gravity *= gravityModifier;
        
        lootCount = 0;
        UpdateLootCount();
    }

    private void Update()
    {
        TryJump();
        UpdateSidesMovement();
        UpdateForwardMovement();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (isGameOver)
            return;
        
        if (collision.gameObject.CompareTag("Ground"))
        {
            isOnGround = true;
            dirtParticle.Play();
        }
        else if (collision.gameObject.CompareTag("Obstacle"))
        {
            isGameOver = true;
            explosionParticle.Play();
            dirtParticle.Stop();
            playerAnimation.SetBool("Death_b", true);
            playerAnimation.SetInteger("DeathType_int", 1);
            playerAudio.PlayOneShot(crashSound, 4.0f);
            Debug.Log("Game Over!");
        } 
        else if (collision.gameObject.CompareTag("Loot"))
        {
            lootCount++;
            UpdateLootCount();
            playerAudio.PlayOneShot(lootSound, 2.0f);
            Destroy(collision.gameObject);
        }
    }

    /// <summary>
    /// Helpers.
    /// </summary>
    
    private void TryJump()
    {
        if (!Input.GetKey(KeyCode.Space) || !isOnGround || isGameOver) 
            return;
        isOnGround = false;
        dirtParticle.Stop();
        playerRigidBody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        playerAnimation.SetTrigger("Jump_trig");
        playerAudio.PlayOneShot(jumpSound, 0.7f);
    }

    private void UpdateSidesMovement()
    {
        if (isGameOver)
            return;
        
        if (transform.position.z < -Z_RANGE)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, -Z_RANGE);
            return;
        }
        
        if (transform.position.z > Z_RANGE)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, Z_RANGE);
            return;
        }
        
        var sizesInput = Input.GetAxis("Vertical");
        
        if (transform.position.z == -Z_RANGE && sizesInput < 0)
            return;
        
        if (transform.position.z == Z_RANGE && sizesInput > 0)
            return;
        
        transform.Translate(Vector3.left * Time.deltaTime * STRAFE_SPEED * sizesInput);
        
    }

    private void UpdateForwardMovement()
    {
        if (isGameOver)
            return;
        
        if (transform.position.x < X_MIN)
        {
            transform.position = new Vector3(X_MIN, transform.position.y, transform.position.z);
            return;
        }
        
        if (transform.position.x > X_MAX)
        {
            transform.position = new Vector3(X_MAX, transform.position.y, transform.position.z);
            return;
        }
        
        var forwardInput = Input.GetAxis("Horizontal");
        
        if (transform.position.x == X_MIN && forwardInput < 0)
            return;
        
        if (transform.position.x == X_MAX && forwardInput > 0)
            return;
        
        transform.Translate(Vector3.forward * Time.deltaTime * STRAFE_SPEED * forwardInput);
    }

    private void UpdateLootCount()
    {
        countText.text = "Loots: " + lootCount;
    }
}
