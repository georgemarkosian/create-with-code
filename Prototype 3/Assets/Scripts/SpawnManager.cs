﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class SpawnManager : MonoBehaviour
{
    private const float SPAWN_POSITION_X = 20f;
    private const float OBSTACLES_SPAWN_START_DELAY = 3f;
    private const float LOOT_SPAWN_START_DELAY = 1f;
    
    public GameObject obstaclePrefab;
    public GameObject lootPrefab;
    
    private readonly float[] spawnPositionsZ = { -3f, -1.5f, 0f, 1.5f, 3f };
    private readonly float[] spawnPositionsY = { 0.4f, 3.5f};
    
    private readonly Tuple<float, float> _intervalRange = new Tuple<float, float>(0.1f, 3f);

    private PlayerController playerControllerScript;
    
    private void Start()
    {
        playerControllerScript = GameObject.Find("Player").GetComponent<PlayerController>();
        Invoke(nameof(SpawnObstacle), OBSTACLES_SPAWN_START_DELAY);
        Invoke(nameof(SpawnLoot), LOOT_SPAWN_START_DELAY);
    }

    private void SpawnObstacle()
    {
        if (playerControllerScript.isGameOver)
            return;

        var index = Random.Range(0, spawnPositionsZ.Length);
        var positions = new Vector3(SPAWN_POSITION_X, 0, spawnPositionsZ[index]);
        Instantiate(obstaclePrefab, positions, obstaclePrefab.transform.rotation);
        Invoke(nameof(SpawnObstacle), Random.Range(_intervalRange.Item1, _intervalRange.Item2));
    }

    private void SpawnLoot()
    {
        if (playerControllerScript.isGameOver)
            return;
        var indexY = Random.Range(0, spawnPositionsY.Length);
        var indexZ = Random.Range(0, spawnPositionsZ.Length);
        var positions = new Vector3(SPAWN_POSITION_X, spawnPositionsY[indexY], spawnPositionsZ[indexZ]);
        Instantiate(lootPrefab, positions, obstaclePrefab.transform.rotation);
        Invoke(nameof(SpawnLoot), Random.Range(_intervalRange.Item1, _intervalRange.Item2));
    }
}
