﻿using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float speed;
    public float destroyY = -10;
    
    private Rigidbody _enemyRigidBody;
    private GameObject _player;
    
    private void Start()
    {
        _enemyRigidBody = GetComponent<Rigidbody>();
        _player = GameObject.Find("Player");
    }

    private void Update()
    {
        if (_player.transform.position.y > 0)
        {
            var position = (_player.transform.position - transform.position).normalized;
            _enemyRigidBody.AddForce(position * speed);    
        }

        if (transform.position.y < destroyY)
            Destroy(gameObject);
    }
}
