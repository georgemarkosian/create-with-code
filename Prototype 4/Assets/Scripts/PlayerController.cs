﻿using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float powerUpStrength;
    public float powerUpDuration;
    public float gameOverY = -10;
    
    public bool isPowerUpActive;
    public bool isGameOver;

    public GameObject powerUpIndicator;

    private Rigidbody _playerRigidBody;
    private GameObject _focalPoint;
    
    /*
     * Unity events.
     */

    private void Start()
    {
        _playerRigidBody = GetComponent<Rigidbody>();
        _focalPoint = GameObject.Find("FocalPoint");
        
        isGameOver = false;
    }

    private void Update()
    {
        if (isGameOver)
            return;
        
        UpdateMovement();
        UpdatePowerUpIndicatorPosition();
        CheckGameOver();
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.CompareTag("PowerUp") && !isPowerUpActive)
        {
            ActivatePowerUp();
            Destroy(collider.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy") && isPowerUpActive)
        {
            var enemyRigidBody = collision.gameObject.GetComponent<Rigidbody>();
            var awayFromPlayerVector = collision.gameObject.transform.position - transform.position;
            enemyRigidBody.AddForce(awayFromPlayerVector * powerUpStrength, ForceMode.Impulse);
        }
    }
    
    /*
     * Movement.
     */

    private void UpdateMovement()
    {
        var forwardInput = Input.GetAxis("Vertical");
        var sidesInput = Input.GetAxis("Horizontal");
        
        _playerRigidBody.AddForce(_focalPoint.transform.forward * forwardInput * speed);
        _playerRigidBody.AddForce(_focalPoint.transform.right * sidesInput * speed);
    }

    /*
     * Power Up.
     */

    private void ActivatePowerUp()
    {
        isPowerUpActive = true;
        powerUpIndicator.SetActive(true);
        StartCoroutine(PowerUpCountdownRoutine());
    }

    private IEnumerator PowerUpCountdownRoutine()
    {
        yield return new WaitForSeconds(powerUpDuration);
        isPowerUpActive = false;
        powerUpIndicator.SetActive(false);
    }

    private void UpdatePowerUpIndicatorPosition()
    {
        if (isPowerUpActive)
            powerUpIndicator.transform.position = transform.position + new Vector3(0, -0.6f, 0);
    }
    
    /*
     * Game Over.
     */

    private void CheckGameOver()
    {
        if (transform.position.y < gameOverY)
        {
            isGameOver = true;
            Debug.Log("Game Over!");
        }
    }
}
