﻿using UnityEngine;

public class RotateCamera : MonoBehaviour
{
    public float rotationSpeed;
    
    private void Update()
    {
        var horizontalInput = Input.GetAxis("CameraRotation");
        transform.Rotate(Vector3.up, horizontalInput * rotationSpeed * Time.deltaTime);
    }
}
