﻿using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    private const float SPAWN_RANGE = 9;
    
    public GameObject enemyPrefab;
    public GameObject powerUpPrefab;
    
    public int enemiesCount;
    public int wave;
    
    private void Start()
    {
        wave = 0;
    }

    private void Update()
    {
        enemiesCount = FindObjectsOfType<EnemyController>().Length;
        if (enemiesCount == 0)
        {
            wave++;
            SpawnEnemies(wave);
            SpawnPowerUp();
        }
    }
    
    /*
     * Enemies.
     */

    private void SpawnEnemies(int count)
    {
        for (var i = 0; i < count; i++)
            SpawnEnemy(GetRandomPosition());
    }

    private void SpawnEnemy(Vector3 position)
    {
        Instantiate(enemyPrefab, position, enemyPrefab.transform.rotation);
    }
    
    /*
     * Power Ups.
     */

    private void SpawnPowerUp()
    {
        SpawnPowerUp(GetRandomPosition());
    }
    
    private void SpawnPowerUp(Vector3 position)
    {
        Instantiate(powerUpPrefab, position, powerUpPrefab.transform.rotation);
    }

    /*
     * Position.
     */
    
    private Vector3 GetRandomPosition()
    {
        var x = Random.Range(-SPAWN_RANGE, SPAWN_RANGE);
        var z = Random.Range(-SPAWN_RANGE, SPAWN_RANGE);
        return new Vector3(x, 0, z);
    }
}
