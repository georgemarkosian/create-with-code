﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    private const float SPAWN_RATE_DEFAULT = 1f;

    public GameObject titleScreen;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI gameOverText;
    public Button restartButton;
    public List<GameObject> targets;

    private float _spawnRate = 1f;
    private int _score;
    private bool _isGameOver;
    
    public bool IsGameOver => _isGameOver;

    /*
     * Public.
     */

    public void StartGame(float difficulty)
    {
        _spawnRate = SPAWN_RATE_DEFAULT / difficulty;
        _score = 0;
        _isGameOver = false;

        HideTitleScreen();
        UpdateScore(0);
        StartCoroutine(SpawnTargets());
    }
    
    public void UpdateScore(int delta)
    {
        if (_isGameOver)
            return;
        _score = Math.Max(_score + delta, 0);
        scoreText.text = $"Score: {_score}";
    }
    
    public void GameOver()
    {
        if (_isGameOver)
            return;
        _isGameOver = true;
        gameOverText.gameObject.SetActive(true);
        restartButton.gameObject.SetActive(true);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    
    /*
     * Spawning.
     */

    private IEnumerator SpawnTargets()
    {
        while (!_isGameOver)
        {
            yield return new WaitForSeconds(_spawnRate);
            var index = Random.Range(0, targets.Count);
            Instantiate(targets[index]);
        }
    }
    
    /*
     * Helpers.
     */

    private void HideTitleScreen()
    {
        titleScreen.gameObject.SetActive(false);
    }
}
