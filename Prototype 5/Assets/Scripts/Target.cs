﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class Target : MonoBehaviour
{
    private const float torqueMax = 10f;
    private const float spawnY = -3;
    
    private readonly Tuple<float, float> speedRange = new Tuple<float, float>(12, 16);
    private readonly Tuple<float, float> spawnXRange = new Tuple<float, float>(-4, 4);

    public ParticleSystem particleExplosion;
    public int pointValue;
    
    private GameManager _gameManager;
    private Rigidbody _targetRigidBody;
    
    private void Start()
    {
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            
        _targetRigidBody = GetComponent<Rigidbody>();
        _targetRigidBody.AddForce(GetRandomForce(), ForceMode.Impulse);
        _targetRigidBody.AddTorque(GetRandomTorque(), GetRandomTorque(), GetRandomTorque(), ForceMode.Impulse);
        
        transform.position = new Vector3(GetRandomX(), spawnY);
    }
    
    /*
     * Mouse Events.
     */

    private void OnMouseDown()
    {
        if (_gameManager.IsGameOver)
            return;
        _gameManager.UpdateScore(pointValue);
        Instantiate(particleExplosion, transform.position, particleExplosion.transform.rotation);
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
        if (gameObject.CompareTag("Bad"))
            _gameManager.GameOver();
    }

    /*
     * Helpers.
     */

    private Vector3 GetRandomForce()
    {
        return Vector3.up * Random.Range(speedRange.Item1, speedRange.Item2);
    }
    
    private float GetRandomTorque()
    {
        return Random.Range(-torqueMax, torqueMax);
    }

    private float GetRandomX()
    {
        return Random.Range(spawnXRange.Item1, spawnXRange.Item2);
    }
}
